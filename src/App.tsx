import { CurrencyRates } from 'currency-rates';
import { useEffect, useState } from 'react';
import './App.css';

const API_KEY = import.meta.env.VITE_API_KEY;

const App = () => {
	const [currencyRates, setCurrencyRates] = useState<CurrencyRates | null>(null);

	useEffect(() => {
		(async () => {
			const response = await fetch(`https://api.currencyfreaks.com/latest?apikey=${API_KEY}&symbols=CAD,IDR,JPY,CHF,EUR,GBP`);
			const data = await response.json();

			setCurrencyRates(data);
		})();
	}, []);

	return (
		<main className="App">
			<table className="currency-table text-lg">
				<thead>
					<tr>
						<th>Currency</th>
						<th>We Buy</th>
						<th>Exchange Rate</th>
						<th>We Sell</th>
					</tr>
				</thead>
				<tbody>
					{currencyRates ? (
						Object.keys(currencyRates.rates).map(rate => {
							const formatRate = parseFloat(currencyRates.rates[rate]).toFixed(4);
							const fivePercen = Number(formatRate) * 0.05;

							return (
								<tr key={rate}>
									<th className="font-normal">{rate}</th>
									<th className="font-normal">{Number(formatRate) + fivePercen}</th>
									<th className="font-normal">{formatRate}</th>
									<th className="font-normal">{(Number(formatRate) - fivePercen).toFixed(4)}</th>
								</tr>
							);
						})
					) : (
						<tr>
							<th rowSpan={4} className="font-normal">
								Loading...
							</th>
						</tr>
					)}
				</tbody>
			</table>
		</main>
	);
};

export default App;
