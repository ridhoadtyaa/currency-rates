declare module 'currency-rates' {
	export interface CurrencyRates {
		date: string;
		base: string;
		rates: typeof Rates;
	}

	export interface Rates {
		CAD: string;
		IDR: string;
		JPY: string;
		CHF: string;
		EUR: string;
		GBP: string;
	}
}
